# Phenix Lodgement API
Lodge a job with Phenix. To gain access to API, contact Ben Riley <ben.riley@phenixjewellery.com>. This specification is the base API, which is customised for insurers on an as needed basis.

## Version: 1.0.0

### /brand

#### GET
##### Summary:

List brands which are registered with Phenix

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [ [Brand](#brand) ] |
| 405 | Invalid input |  |
| 500 | Server side error |  |

#### POST
##### Summary:

Register brand with phenix API

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [Brand](#brand) |
| 405 | Invalid input |  |
| 500 | Server side error |  |

### /brand/{brandId}/job

#### POST
##### Summary:

Add a new job against a registered brand.

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body | Job to add | Yes | [Job](#job) |
| brandId | path | ID of brand | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [Job](#job) |
| 404 | Job/brand not found |  |
| 405 | Invalid input |  |
| 500 | Server side error |  |

### /brand/{brandId}/job/{jobId}

#### GET
##### Summary:

Get a job by ID

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| jobId | path | ID of job | Yes | string |
| brandId | path | ID of brand | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [Job](#job) |
| 400 | Invalid Job supplied |  |
| 404 | Job/brand not found |  |
| 500 | Server side error |  |

### /brand/{brandId}/job/{jobId}/item

#### POST
##### Summary:

Add an item to a job

##### Description:



##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| body | body |  | Yes | [Item](#item) |
| jobId | path | ID of job | Yes | string |
| brandId | path | ID of brand | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [ApiResponse](#apiresponse) |
| 404 | Job/brand not found |  |
| 405 | Invalid input |  |
| 500 | Server side error |  |

### /brand/{brandId}/job/{jobId}/attachment

#### POST
##### Summary:

Uploads a file against a job

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| attachment | formData | The file to upload. | No | file |
| type | formData | The file to upload. | No | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [ApiResponse](#apiresponse) |
| 404 | Job/brand not found |  |
| 405 | Invalid input |  |
| 500 | Server side error |  |

### /brand/{brandId}/job/{jobId}/lodge

#### POST
##### Summary:

When all data for job is uploaded, request the job to be processed by phenix.

##### Description:

Lodge a job with phenix

##### Parameters

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| jobId | path | ID of job | Yes | string |
| brandId | path | ID of brand | Yes | string |

##### Responses

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 |  | [ApiResponse](#apiresponse) |
| 404 | Job/brand not found |  |
| 405 | Invalid input |  |
| 500 | Server side error |  |

### Models


#### Item

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | long |  | No |
| summary | string |  | No |
| description | string |  | No |
| type | string |  | No |
| quantity | integer |  | No |
| brand | string |  | No |

#### Job

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | string |  | No |
| approvalAmount | number (decimal) |  | No |
| status | string |  | No |
| notes | string |  | No |
| policyCreationDate | date |  | No |
| primaryContactName | string |  | No |
| alternateContactName | string |  | No |
| excess | number |  | No |
| perItemLimit | number |  | No |
| totalPolicyLimit | number |  | No |
| claimNumber | string |  | No |
| insuranceReferenceNumber | string |  | No |
| emailAddress | string |  | No |
| streetAddress | string |  | No |
| postcode | string |  | No |
| suburb | string |  | No |
| state | string |  | No |
| country | string |  | No |
| phone | string |  | No |
| phone1 | string |  | No |
| phone2 | string |  | No |

#### Brands

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| id | string |  | No |
| name | string |  | Yes |

#### ApiResponse

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| code | integer |  | No |
| type | string |  | No |
| message | string |  | No |